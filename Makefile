########################################################################
#
# Makefile for building the data structure invariant detector
#
#  Example uses:    
#       gnumake JDK=<java_home> OSNAME=solaris [OPT=true] [LIBARCH=sparc]
#       gnumake JDK=<java_home> OSNAME=solaris [OPT=true] [LIBARCH=sparcv9]
#       gnumake JDK=<java_home> OSNAME=linux   [OPT=true]
#       gnumake JDK=<java_home> OSNAME=win32   [OPT=true]
#
########################################################################
# for java_crw_demo
OSNAME = linux
JDK = $(JAVA_HOME)
LIBARCH = amd64

# Source lists
LIBNAME=taggingExample
SOURCES=./src/cpp/objNumber.c ./agent_util/agent_util.c
#JAVA_SOURCES=./src/java/HeapTracker.java
JAVA_SOURCES=./HeapTracker.java

# Name of jar file that needs to be created
JARFILE=heapTracker.jar

print-% : ; @echo $* = $($*)



# Linux GNU C Compiler
ifeq ($(OSNAME), linux)
    # GNU Compiler options needed to build it
    COMMON_FLAGS=-fno-strict-aliasing -fPIC -fno-omit-frame-pointer
    # Options that help find errors
    COMMON_FLAGS+= -W -Wall  -Wno-unused -Wno-parentheses
    ifeq ($(OPT), true)
        CFLAGS=-O2 $(COMMON_FLAGS) 
    else
        CFLAGS=-g $(COMMON_FLAGS) 
    endif
    # Object files needed to create library
    OBJECTS=$(SOURCES:%.c=%.o)
    # Library name and options needed to build it
    LIBRARY=lib$(LIBNAME).so
    LDFLAGS=-Wl,-soname=$(LIBRARY) -static-libgcc #-mimpure-text
    # Libraries we are dependent on
    LIBRARIES=-L $(JDK)/jre/lib/$(LIBARCH) -ljava_crw_demo -lc
    # Building a shared library
    LINK_SHARED=$(LINK.c) -shared -o $@
endif


# Common -I options
CFLAGS += -I.
CFLAGS += -I./agent_util
CFLAGS += -I./java_crw_demo
CFLAGS += -I$(JDK)/include -I$(JDK)/include/$(OSNAME)

# Default rule (build both native library and jar file)
all: $(LIBRARY) $(JARFILE)

# Build native library
$(LIBRARY): $(OBJECTS)
	$(LINK_SHARED) $(OBJECTS) $(LIBRARIES)

# Build jar file
$(JARFILE): $(JAVA_SOURCES)
	rm -f -r classes
	mkdir -p classes
	$(JDK)/bin/javac -d classes $(JAVA_SOURCES)
	(cd classes; $(JDK)/bin/jar cf ../$@ *)

# Cleanup the built bits
clean:
	rm -f -r classes
	rm -f $(LIBRARY) $(JARFILE) $(OBJECTS)

# Simple tester
#home/wbj/jvmtiproject
test: all
	 LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp .:/home/bobo/Dropbox/PerfBlower/jomp1.0b.jar:${PWD}/bench/v2.0:${PWD}/bench/v2.0/section3:${PWD}/bench/ JGFRayTracerBenchSizeA

	# LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar hsqldb #-version
#	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar /home/bobo/Desktop/dacapo-2006-10-MR2.jar  hsqldb #-version

chart:	all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar chart #-version
eclipse: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar eclipse #-version
fop: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar fop #-version
hsqldb:	all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar hsqldb #-version
lusearch: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar lusearch #-version
luindex: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar luindex #-version
pmd: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar pmd #-version
xalan: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar xalan #-version
antlr: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar antlr #-version
bloat: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar bloat #-version
jython: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -agentlib:$(LIBNAME) -Xbootclasspath/a:./$(JARFILE) -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar jython #-version
antlr2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar antlr
bloat2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar bloat
chart2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar chart
eclipse2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar eclipse
fop2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar fop
hsqkdb2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar hsqldb
jython2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar jython
luindex2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar luindex
lusearch2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar lusearch
pmd2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar pmd
xalan2: all
	LD_LIBRARY_PATH=. $(JDK)/bin/java -cp $(PWD)/bench -jar ${PWD}/bench/dacapo-2006-10-MR2.jar xalan

# Compilation rule only needed on Windows
ifeq ($(OSNAME), win32)
%.obj: %.c
	$(COMPILE.c) $<
endif



# Windows Microsoft C/C++ Optimizing Compiler Version 12
ifeq ($(OSNAME), win32)
    CC=cl
    # Compiler options needed to build it
    COMMON_FLAGS=-Gy -DWIN32
    # Options that help find errors
    COMMON_FLAGS+=-W0 -WX
    ifeq ($(OPT), true)
        CFLAGS= -Ox -Op -Zi $(COMMON_FLAGS) 
    else
        CFLAGS= -Od -Zi $(COMMON_FLAGS) 
    endif
    # Sources need java_crw_demo
    SOURCES += ../java_crw_demo/java_crw_demo.c
    # Object files needed to create library
    OBJECTS=$(SOURCES:%.c=%.obj)
    # Library name and options needed to build it
    LIBRARY=$(LIBNAME).dll
    LDFLAGS=
    # Libraries we are dependent on
    LIBRARIES=$(JDK)/
    # Building a shared library
    LINK_SHARED=link -dll -out:$@
endif

# Solaris Sun C Compiler Version 5.5
ifeq ($(OSNAME), solaris)
    # Sun Solaris Compiler options needed
    COMMON_FLAGS=-mt -KPIC
    # Options that help find errors
    COMMON_FLAGS+= -Xa -v -xstrconst -xc99=%none
    # Check LIBARCH for any special compiler options
    LIBARCH=amd64 
    ifeq ($(LIBARCH), sparc)
        COMMON_FLAGS+=-xarch=v8 -xregs=no%appl
    endif
    ifeq ($(LIBARCH), sparcv9)
        COMMON_FLAGS+=-xarch=v9 -xregs=no%appl
    endif
    ifeq ($(OPT), true)
        CFLAGS=-xO2 $(COMMON_FLAGS) 
    else
        CFLAGS=-g $(COMMON_FLAGS)
    endif
    # Object files needed to create library
    OBJECTS=$(SOURCES:%.c=%.o)
    # Library name and options needed to build it
    LIBRARY=lib$(LIBNAME).so
    LDFLAGS=-z defs -ztext
    # Libraries we are dependent on
    LIBRARIES=-L $(JDK)/jre/lib/$(LIBARCH) -ljava_crw_demo -lc
    # Building a shared library
    LINK_SHARED=$(LINK.c) -G -o $@
endif
























