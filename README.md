# README #
The project is hosted on Bitbucket: [https://bitbucket.org/J7Island/jvmtiproject/](https://bitbucket.org/J7Island/jvmtiproject/)

### What is this repository for? ###
- It's a part of the research project PerfBlower.
- It's a data structure invariant detector. A data structure invariant problem is a frequently-occurring coding pattern that may lead to large volumes of (temporary) objects allocated per loop iteration, but these objects contains values independent of specific iterations.
- This detector can help programers to detect data structure invariant problems and improve the performance of the programs at the development phase.

### How to compile ###
In directory "jvmtiproject":
```sh
$ make all
```

#### Test the benchmark with/without data structure invariant detector ####
We use the *dacapo-2006-10-MR2* as our benchmarks. They are *chart*, *eclipse*, *fop*, *hsqldb*, *lusearch*, *luindex*, *pmd*, *xalan*, *antlr*, *bloat*, *jython*.

To run the benchmark (e.g. antlr) without the detector:
```sh
$ make antlr2
```

To run the benchmark (e.g. antlr) with the detector:
```sh
$ make antlr
```
