<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
<head>
<link rel="icon" href="/jdk7/jdk7/jdk/static/nanoduke.ico" />
<meta name="robots" content="index, nofollow" />
<link rel="stylesheet" href="/jdk7/jdk7/jdk/static/style-paper.css" type="text/css" />
<script type="text/javascript" src="/jdk7/jdk7/jdk/static/mercurial.js"></script>

<title>jdk7/jdk7/jdk: 9b8c96f96a0f src/share/demo/jvmti/agent_util/README.txt</title>
</head>
<body>

<div class="container">
<div class="menu">
<div class="logo"><img src="/jdk7/jdk7/jdk/static/duke-pipes.png" border=0 alt="Duke!"/></div>
<ul>
<li><a href="/jdk7/jdk7/jdk/shortlog/9b8c96f96a0f">log</a></li>
<li><a href="/jdk7/jdk7/jdk/graph/9b8c96f96a0f">graph</a></li>
<li><a href="/jdk7/jdk7/jdk/tags">tags</a></li>
<li><a href="/jdk7/jdk7/jdk/branches">branches</a></li>
</ul>
<ul>
<li><a href="/jdk7/jdk7/jdk/rev/9b8c96f96a0f">changeset</a></li>
<li><a href="/jdk7/jdk7/jdk/file/9b8c96f96a0f/src/share/demo/jvmti/agent_util/">browse</a></li>
</ul>
<ul>
<li class="active">file</li>
<li><a href="/jdk7/jdk7/jdk/file/tip/src/share/demo/jvmti/agent_util/README.txt">latest</a></li>
<li><a href="/jdk7/jdk7/jdk/diff/9b8c96f96a0f/src/share/demo/jvmti/agent_util/README.txt">diff</a></li>
<li><a href="/jdk7/jdk7/jdk/comparison/9b8c96f96a0f/src/share/demo/jvmti/agent_util/README.txt">comparison</a></li>
<li><a href="/jdk7/jdk7/jdk/annotate/9b8c96f96a0f/src/share/demo/jvmti/agent_util/README.txt">annotate</a></li>
<li><a href="/jdk7/jdk7/jdk/log/9b8c96f96a0f/src/share/demo/jvmti/agent_util/README.txt">file log</a></li>
<li><a href="/jdk7/jdk7/jdk/raw-file/9b8c96f96a0f/src/share/demo/jvmti/agent_util/README.txt">raw</a></li>
</ul>
<ul>
<li><a href="/jdk7/jdk7/jdk/help">help</a></li>
</ul>
</div>

<div class="main">
<h2 class="breadcrumb"><a href="/">OpenJDK</a> / <a href="/jdk7">jdk7</a> / <a href="/jdk7/jdk7">jdk7</a> / <a href="/jdk7/jdk7/jdk">jdk</a> </h2>
<h3>view src/share/demo/jvmti/agent_util/README.txt @ 4338:9b8c96f96a0f</h3>

<form class="search" action="/jdk7/jdk7/jdk/log">

<p><input name="rev" id="search1" type="text" size="30" /></p>
<div id="hint">Find changesets by keywords (author, files, the commit message), revision
number or hash, or <a href="/jdk7/jdk7/jdk/help/revsets">revset expression</a>.</div>
</form>

<div class="description">Added tag jdk7-b147 for changeset f097ca2434b1</div>

<table id="changesetEntry">
<tr>
 <th class="author">author</th>
 <td class="author">&#115;&#99;&#104;&#105;&#101;&#110;</td>
</tr>
<tr>
 <th class="date">date</th>
 <td class="date age">Mon, 27 Jun 2011 13:21:34 -0700</td>
</tr>
<tr>
 <th class="author">parents</th>
 <td class="author"><a href="/jdk7/jdk7/jdk/file/37a05a11f281/src/share/demo/jvmti/agent_util/README.txt">37a05a11f281</a> </td>
</tr>
<tr>
 <th class="author">children</th>
 <td class="author"></td>
</tr>
</table>

<div class="overflow">
<div class="sourcefirst linewraptoggle">line wrap: <a class="linewraplink" href="javascript:toggleLinewrap()">on</a></div>
<div class="sourcefirst"> line source</div>
<pre class="sourcelines stripes4 wrap">
<span id="l1">#</span><a href="#l1"></a>
<span id="l2"># Copyright (c) 2004, 2007, Oracle and/or its affiliates. All rights reserved.</span><a href="#l2"></a>
<span id="l3">#</span><a href="#l3"></a>
<span id="l4"># Redistribution and use in source and binary forms, with or without</span><a href="#l4"></a>
<span id="l5"># modification, are permitted provided that the following conditions</span><a href="#l5"></a>
<span id="l6"># are met:</span><a href="#l6"></a>
<span id="l7">#</span><a href="#l7"></a>
<span id="l8">#   - Redistributions of source code must retain the above copyright</span><a href="#l8"></a>
<span id="l9">#     notice, this list of conditions and the following disclaimer.</span><a href="#l9"></a>
<span id="l10">#</span><a href="#l10"></a>
<span id="l11">#   - Redistributions in binary form must reproduce the above copyright</span><a href="#l11"></a>
<span id="l12">#     notice, this list of conditions and the following disclaimer in the</span><a href="#l12"></a>
<span id="l13">#     documentation and/or other materials provided with the distribution.</span><a href="#l13"></a>
<span id="l14">#</span><a href="#l14"></a>
<span id="l15">#   - Neither the name of Oracle nor the names of its</span><a href="#l15"></a>
<span id="l16">#     contributors may be used to endorse or promote products derived</span><a href="#l16"></a>
<span id="l17">#     from this software without specific prior written permission.</span><a href="#l17"></a>
<span id="l18">#</span><a href="#l18"></a>
<span id="l19"># THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS</span><a href="#l19"></a>
<span id="l20"># IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,</span><a href="#l20"></a>
<span id="l21"># THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR</span><a href="#l21"></a>
<span id="l22"># PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR</span><a href="#l22"></a>
<span id="l23"># CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,</span><a href="#l23"></a>
<span id="l24"># EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,</span><a href="#l24"></a>
<span id="l25"># PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR</span><a href="#l25"></a>
<span id="l26"># PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF</span><a href="#l26"></a>
<span id="l27"># LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING</span><a href="#l27"></a>
<span id="l28"># NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS</span><a href="#l28"></a>
<span id="l29"># SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</span><a href="#l29"></a>
<span id="l30">#</span><a href="#l30"></a>
<span id="l31"></span><a href="#l31"></a>
<span id="l32">agent_util sources</span><a href="#l32"></a>
<span id="l33"></span><a href="#l33"></a>
<span id="l34">Just some shared generic source used by several of the demos.</span><a href="#l34"></a>
<span id="l35"></span><a href="#l35"></a></pre>
<div class="sourcelast"></div>
</div>
</div>
</div>

<script type="text/javascript">process_dates()</script>


<div class="container"><div class="main footer">
&copy 2007, 2017 Oracle and/or its affiliates<br/>
<a href="http://openjdk.java.net/legal/tou/">Terms of Use</a>
&#183; <a href="http://www.oracle.com/us/legal/privacy/">Privacy</a>
&#183; <a href="http://www.oracle.com/us/legal/third-party-trademarks/third-party-trademarks-078568.html">Trademarks</a>
</div></div>

</body>
</html>

