import java.util.ArrayList;

public class Demo {
    public static ArrayList<Demo> list = new ArrayList<Demo>();
    public int i = 0;
    public double j = 0.0;
    public static void bar() {
	for (int i = 0; i < 1000; i++) {
	    // System.out.println("Allocated an object!");
	    Demo a = new Demo();
	    a.i = 1;
	    a.j = 3.7;
	    Demo.list.add(a);
	    for (Demo a2 : list) {
		int ai = a2.i;
		double aj = a2.j;
	    }
	    if (i % 100 == 0) System.gc();
	}

    }
    public static void foo() {
	bar();
    }
    public static void main(String[] args) {
	foo();
    }
}
