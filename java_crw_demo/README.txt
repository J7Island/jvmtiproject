<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
<head>
<link rel="icon" href="/jdk7/jdk7/jdk/static/nanoduke.ico" />
<meta name="robots" content="index, nofollow" />
<link rel="stylesheet" href="/jdk7/jdk7/jdk/static/style-paper.css" type="text/css" />
<script type="text/javascript" src="/jdk7/jdk7/jdk/static/mercurial.js"></script>

<title>jdk7/jdk7/jdk: 9b8c96f96a0f src/share/demo/jvmti/java_crw_demo/README.txt</title>
</head>
<body>

<div class="container">
<div class="menu">
<div class="logo"><img src="/jdk7/jdk7/jdk/static/duke-pipes.png" border=0 alt="Duke!"/></div>
<ul>
<li><a href="/jdk7/jdk7/jdk/shortlog/9b8c96f96a0f">log</a></li>
<li><a href="/jdk7/jdk7/jdk/graph/9b8c96f96a0f">graph</a></li>
<li><a href="/jdk7/jdk7/jdk/tags">tags</a></li>
<li><a href="/jdk7/jdk7/jdk/branches">branches</a></li>
</ul>
<ul>
<li><a href="/jdk7/jdk7/jdk/rev/9b8c96f96a0f">changeset</a></li>
<li><a href="/jdk7/jdk7/jdk/file/9b8c96f96a0f/src/share/demo/jvmti/java_crw_demo/">browse</a></li>
</ul>
<ul>
<li class="active">file</li>
<li><a href="/jdk7/jdk7/jdk/file/tip/src/share/demo/jvmti/java_crw_demo/README.txt">latest</a></li>
<li><a href="/jdk7/jdk7/jdk/diff/9b8c96f96a0f/src/share/demo/jvmti/java_crw_demo/README.txt">diff</a></li>
<li><a href="/jdk7/jdk7/jdk/comparison/9b8c96f96a0f/src/share/demo/jvmti/java_crw_demo/README.txt">comparison</a></li>
<li><a href="/jdk7/jdk7/jdk/annotate/9b8c96f96a0f/src/share/demo/jvmti/java_crw_demo/README.txt">annotate</a></li>
<li><a href="/jdk7/jdk7/jdk/log/9b8c96f96a0f/src/share/demo/jvmti/java_crw_demo/README.txt">file log</a></li>
<li><a href="/jdk7/jdk7/jdk/raw-file/9b8c96f96a0f/src/share/demo/jvmti/java_crw_demo/README.txt">raw</a></li>
</ul>
<ul>
<li><a href="/jdk7/jdk7/jdk/help">help</a></li>
</ul>
</div>

<div class="main">
<h2 class="breadcrumb"><a href="/">OpenJDK</a> / <a href="/jdk7">jdk7</a> / <a href="/jdk7/jdk7">jdk7</a> / <a href="/jdk7/jdk7/jdk">jdk</a> </h2>
<h3>view src/share/demo/jvmti/java_crw_demo/README.txt @ 4338:9b8c96f96a0f</h3>

<form class="search" action="/jdk7/jdk7/jdk/log">

<p><input name="rev" id="search1" type="text" size="30" /></p>
<div id="hint">Find changesets by keywords (author, files, the commit message), revision
number or hash, or <a href="/jdk7/jdk7/jdk/help/revsets">revset expression</a>.</div>
</form>

<div class="description">Added tag jdk7-b147 for changeset f097ca2434b1</div>

<table id="changesetEntry">
<tr>
 <th class="author">author</th>
 <td class="author">&#115;&#99;&#104;&#105;&#101;&#110;</td>
</tr>
<tr>
 <th class="date">date</th>
 <td class="date age">Mon, 27 Jun 2011 13:21:34 -0700</td>
</tr>
<tr>
 <th class="author">parents</th>
 <td class="author"><a href="/jdk7/jdk7/jdk/file/37a05a11f281/src/share/demo/jvmti/java_crw_demo/README.txt">37a05a11f281</a> </td>
</tr>
<tr>
 <th class="author">children</th>
 <td class="author"></td>
</tr>
</table>

<div class="overflow">
<div class="sourcefirst linewraptoggle">line wrap: <a class="linewraplink" href="javascript:toggleLinewrap()">on</a></div>
<div class="sourcefirst"> line source</div>
<pre class="sourcelines stripes4 wrap">
<span id="l1">#</span><a href="#l1"></a>
<span id="l2"># Copyright (c) 2004, 2007, Oracle and/or its affiliates. All rights reserved.</span><a href="#l2"></a>
<span id="l3">#</span><a href="#l3"></a>
<span id="l4"># Redistribution and use in source and binary forms, with or without</span><a href="#l4"></a>
<span id="l5"># modification, are permitted provided that the following conditions</span><a href="#l5"></a>
<span id="l6"># are met:</span><a href="#l6"></a>
<span id="l7">#</span><a href="#l7"></a>
<span id="l8">#   - Redistributions of source code must retain the above copyright</span><a href="#l8"></a>
<span id="l9">#     notice, this list of conditions and the following disclaimer.</span><a href="#l9"></a>
<span id="l10">#</span><a href="#l10"></a>
<span id="l11">#   - Redistributions in binary form must reproduce the above copyright</span><a href="#l11"></a>
<span id="l12">#     notice, this list of conditions and the following disclaimer in the</span><a href="#l12"></a>
<span id="l13">#     documentation and/or other materials provided with the distribution.</span><a href="#l13"></a>
<span id="l14">#</span><a href="#l14"></a>
<span id="l15">#   - Neither the name of Oracle nor the names of its</span><a href="#l15"></a>
<span id="l16">#     contributors may be used to endorse or promote products derived</span><a href="#l16"></a>
<span id="l17">#     from this software without specific prior written permission.</span><a href="#l17"></a>
<span id="l18">#</span><a href="#l18"></a>
<span id="l19"># THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS</span><a href="#l19"></a>
<span id="l20"># IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,</span><a href="#l20"></a>
<span id="l21"># THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR</span><a href="#l21"></a>
<span id="l22"># PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR</span><a href="#l22"></a>
<span id="l23"># CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,</span><a href="#l23"></a>
<span id="l24"># EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,</span><a href="#l24"></a>
<span id="l25"># PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR</span><a href="#l25"></a>
<span id="l26"># PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF</span><a href="#l26"></a>
<span id="l27"># LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING</span><a href="#l27"></a>
<span id="l28"># NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS</span><a href="#l28"></a>
<span id="l29"># SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</span><a href="#l29"></a>
<span id="l30">#</span><a href="#l30"></a>
<span id="l31"></span><a href="#l31"></a>
<span id="l32">java_crw_demo Library</span><a href="#l32"></a>
<span id="l33"></span><a href="#l33"></a>
<span id="l34">The library java_crw_demo is a small C library that is used by HPROF</span><a href="#l34"></a>
<span id="l35">and other agent libraries to do some very basic bytecode </span><a href="#l35"></a>
<span id="l36">insertion (BCI) of class files.  This is not an agent </span><a href="#l36"></a>
<span id="l37">library but a general purpose library that can be used to do some </span><a href="#l37"></a>
<span id="l38">very limited bytecode insertion.</span><a href="#l38"></a>
<span id="l39"></span><a href="#l39"></a>
<span id="l40">In the demo sources, look for the use of java_crw_demo.h and</span><a href="#l40"></a>
<span id="l41">the C function java_crw_demo().  The java_crw_demo library is provided </span><a href="#l41"></a>
<span id="l42">as part of the JRE.</span><a href="#l42"></a>
<span id="l43"></span><a href="#l43"></a>
<span id="l44">The basic BCI that this library does includes:</span><a href="#l44"></a>
<span id="l45"></span><a href="#l45"></a>
<span id="l46">    * On entry to the java.lang.Object init method (signature &quot;()V&quot;), </span><a href="#l46"></a>
<span id="l47">      a invokestatic call to tclass.obj_init_method(object); is inserted. </span><a href="#l47"></a>
<span id="l48"></span><a href="#l48"></a>
<span id="l49">    * On any newarray type opcode, immediately following it, the array </span><a href="#l49"></a>
<span id="l50">      object is duplicated on the stack and an invokestatic call to</span><a href="#l50"></a>
<span id="l51">      tclass.newarray_method(object); is inserted. </span><a href="#l51"></a>
<span id="l52"></span><a href="#l52"></a>
<span id="l53">    * On entry to all methods, a invokestatic call to </span><a href="#l53"></a>
<span id="l54">      tclass.call_method(cnum,mnum); is inserted. The agent can map the </span><a href="#l54"></a>
<span id="l55">      two integers (cnum,mnum) to a method in a class, the cnum is the </span><a href="#l55"></a>
<span id="l56">      number provided to the java_crw_demo library when the classfile was </span><a href="#l56"></a>
<span id="l57">      modified.</span><a href="#l57"></a>
<span id="l58"></span><a href="#l58"></a>
<span id="l59">    * On return from any method (any return opcode), a invokestatic call to</span><a href="#l59"></a>
<span id="l60">      tclass.return_method(cnum,mnum); is inserted.  </span><a href="#l60"></a>
<span id="l61"></span><a href="#l61"></a>
<span id="l62">Some methods are not modified at all, init methods and finalize methods </span><a href="#l62"></a>
<span id="l63">whose length is 1 will not be modified.  Classes that are designated </span><a href="#l63"></a>
<span id="l64">&quot;system&quot; will not have their clinit methods modified. In addition, the </span><a href="#l64"></a>
<span id="l65">method java.lang.Thread.currentThread() is not modified.</span><a href="#l65"></a>
<span id="l66"></span><a href="#l66"></a>
<span id="l67">No methods or fields will be added to any class, however new constant </span><a href="#l67"></a>
<span id="l68">pool entries will be added at the end of the original constant pool table.</span><a href="#l68"></a>
<span id="l69">The exception, line, and local variable tables for each method is adjusted </span><a href="#l69"></a>
<span id="l70">for the modification. The bytecodes are compressed to use smaller offsets </span><a href="#l70"></a>
<span id="l71">and the fewest 'wide' opcodes. </span><a href="#l71"></a>
<span id="l72"></span><a href="#l72"></a>
<span id="l73">All attempts are made to minimize the number of bytecodes at each insertion </span><a href="#l73"></a>
<span id="l74">site, however, classes with N return opcodes or N newarray opcodes will get </span><a href="#l74"></a>
<span id="l75">N insertions.  And only the necessary modification dictated by the input </span><a href="#l75"></a>
<span id="l76">arguments to java_crw_demo are actually made.</span><a href="#l76"></a>
<span id="l77"></span><a href="#l77"></a></pre>
<div class="sourcelast"></div>
</div>
</div>
</div>

<script type="text/javascript">process_dates()</script>


<div class="container"><div class="main footer">
&copy 2007, 2017 Oracle and/or its affiliates<br/>
<a href="http://openjdk.java.net/legal/tou/">Terms of Use</a>
&#183; <a href="http://www.oracle.com/us/legal/privacy/">Privacy</a>
&#183; <a href="http://www.oracle.com/us/legal/third-party-trademarks/third-party-trademarks-078568.html">Trademarks</a>
</div></div>

</body>
</html>

