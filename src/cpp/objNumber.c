#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "jvmti.h"
#include "jni.h"
//#include "objNumber.h"

#include "heapTracker.h"
#include "java_crw_demo.h"
#include "agent_util.h"

#define HEAP_TRACKER_class   HeapTracker
#define HEAP_TRACKER_newobj  newobj
#define HEAP_TRACKER_newarr  newarr
#define HEAP_TRACKER_native_newobj _newobj
#define HEAP_TRACKER_native_newarr _newarr
#define HEAP_TRACKER_engaged  engaged

#define _STRING(s) #s
#define STRING(s) _STRING(s)
//#define DEBUG




jvmtiEnv* jvmtiEnvP=NULL;


typedef enum {
  TRACE_FIRST = 0,
  TRACE_USER = 0,
  TRACE_BEFORE_VM_START = 1,
  TRACE_BEFORE_VM_INIT = 2,
  TRACE_VM_OBJECT = 3,
  TRACE_MYSTERY = 4,
  TRACE_LAST = 4
} TraceFlavor;

static char *flavorDesc[] = {
  "",
  "before VM_START",
  "before VM_INIT",
  "VM_OBJECT",
  "unknown"
};

/* Trace (Stack Trace) */
#define MAX_FRAMES 50
typedef struct Trace {
  jint nframes; /* Number of frames */
  jvmtiFrameInfo frames[MAX_FRAMES+2]; // Frames from GetStackTrace()
  TraceFlavor flavor;
} Trace;


//bobo
typedef struct {
  jvmtiHeapReferenceInfo id;
  jvalue value;
} FieldPair;



typedef struct {
  FieldPair** fields;
  int size;
} ObjValue;


typedef struct {
  ObjValue** valuesP;
  int size;
} History;



/* Trace information */
typedef struct TraceInfo {
  Trace trace; /* Trace where this object was allocate from */
  jlong hashCode;   /* 64 bit hash code that attempts to identify this specific trace */
  jlong totalSpace; /* Total space taken up by objects allocated from this trace */
  int totalCount; /*Total count of objects ever allocated from this trace */
  int useCount; /* Total live objects that were allocated from this trace */
  struct TraceInfo *next;

  //bobo
  History* historyP;
  int sameObj; // bad allocation site
} TraceInfo;


// struct to record the tinfo whoes panalty exceeds the threshold
typedef struct TraceInfoRecord {
  TraceInfo* tinfoP;
  int panalty;
  struct TraceInfoRecord* next;
} TraceInfoRecord;


// define OTag data structure
typedef struct {
  TraceInfo* tinfoP;
  ObjValue* currentValue;
  int panalty;
  int whereCreated; 
} OTag;


typedef struct {
  /* JVMTI Environment */
  jvmtiEnv *jvmti;
  JNIEnv * jni;
  jboolean vmStarted;
  jboolean vmInitialized;
  jboolean vmDead;

  /* Options */
  int maxDump;


  // bobo: panalty threshold
  int PANALTY_THRESHOLD;
  //bobo:BAD_ALLOCSITE_THRESHOLD
  int BAD_ALLOCSITE_THRESHOLD;

  /* Data access Lock */
  jrawMonitorID lock;

  //lock for gc arbitary operations
  jrawMonitorID lock2;
  
  JavaVM* jvm;

  int totalObjNum; // bobo-total object number in round 2's iterate
  int badObjNum;
  int totalObjNum3; // total object number in round 3's iterate
  int badObjNum3;
  int obj_count;  //bobo
  int gc_count;
  int gc_number; // bobo: to count how many gc are there;
  int bad_alloc_num; // the bad alloc site during gc;

  TraceInfoRecord** traceInfoTable;

  /* counter how many classes BCI has been applied on */
  jint ccount;
  /* Hash table to lookup TraceInfo's via Trace's */
#define HASH_INDEX_BIT_WIDTH 12 /*4096*/
#define HASH_BUCKET_COUNT (1<<HASH_INDEX_BIT_WIDTH)
#define HASH_INDEX_MASK (HASH_BUCKET_COUNT-1)
  TraceInfo *hashBuckets[HASH_BUCKET_COUNT];
  /* Count of TraceInfo's allocated */
  int traceInfoCount;
  /* Pre-define traces for the system and mystery situations */
  TraceInfo *emptyTrace[TRACE_LAST+1];
  
  //Bobo-debug2;
  int heapSize;
  int totalPanalty;
  double maxVSO;
  int tempCount;
  
} GlobalAgentData;


static GlobalAgentData *gdata;



/* Enter a critical section by doing a JVMTI Raw Monitor Enter */
static void
enterCriticalSection(jvmtiEnv *jvmti)
{
  jvmtiError error;
  error = (*jvmti)->RawMonitorEnter(jvmti, gdata->lock);
  check_jvmti_error(jvmti, error, "bobo-Cannot enter with raw monitor");
}

/* Exit a critical section*/
static void
exitCriticalSection(jvmtiEnv *jvmti)
{
  jvmtiError error;
  error = (*jvmti)->RawMonitorExit(jvmti, gdata->lock);
  check_jvmti_error(jvmti, error, "bobo-Cannot exit with raw monitor");
}

// Implementation of _setTag JNI function.
/* JNIEXPORT static void JNICALL setObjExpression(JNIEnv *env, jclass klass, */
/* 					       jobject o, jobject expr) { */
/*   printf( "************Entered setObjExpression***************************\n"); */
/*   if(gdata->vmDead) */
/*     return; */
/*   if(!o) */
/*     return; */
/*   jvmtiError error; */
/*   jlong tag; */
/*   if (expr) { */
/*     //First see if there's already something set here */
/*     error =gdata->jvmti->GetTag(o,&tag); */
/*     if (tag) */
/*       { */
/* 	//Delete reference to old thing */
/* 	env->DeleteGlobalRef((jobject)(ptrdiff_t) tag); */
/*       } */
/*     //Set the tag, make a new global reference to it */
/*     error = gdata->jvmti->SetTag(o, (jlong) (ptrdiff_t) (void*) env->NewGlobalRef(expr)); */
/*   } */
/*   else { */
/*     error = gdata->jvmti->SetTag(o, 0); */
/*   } */
/*   if (error == JVMTI_ERROR_WRONG_PHASE) */
/*     return; */
/*   check_jvmti_error(gdata->jvmti, error, "Cannot set object tag"); */
/* } */

/* /\* */
/*  * Implementation of _getTag JNI function */
/*  *\/ */
/* JNIEXPORT static jobject JNICALL getObjExpression(JNIEnv *env, jclass klass, */
/* 						  jobject o) { */
/*   if (gdata->vmDead) { */
/*     return NULL; */
/*   } */
/*   jvmtiError error; */
/*   jlong tag; */
/*   error = gdata->jvmti->GetTag(o, &tag); */
/*   if(error == JVMTI_ERROR_WRONG_PHASE) */
/*     return NULL; */
/*   check_jvmti_error(gdata->jvmti, error, "Cannot get object tag"); */
/*   if(tag) */
/*     { */
/*       return (jobject) (ptrdiff_t) tag; */
/*     } */
/*   return NULL; */
/* } */






/*Update stats on a TraceInfo */
static TraceInfo *
updateStats(TraceInfo *tinfo)
{
  tinfo->totalCount++;
  tinfo->useCount++;
  return tinfo;
}




/* Get TraceInfo for empty stack */
static TraceInfo*
emptyTrace(TraceFlavor flavor)
{
  return updateStats(gdata->emptyTrace[flavor]);
}



/* Allocate new TraceInfo */
static TraceInfo *
newTraceInfo(Trace *trace, jlong hashCode, TraceFlavor flavor)
{
  TraceInfo *tinfo;

  tinfo = (TraceInfo*)calloc(1, sizeof(TraceInfo));
  if (tinfo == NULL) {
    fatal_error("ERROR: Ran out of malloc() space\n");
  } else {
    int hashIndex;

    tinfo->trace = *trace;
    tinfo->trace.flavor = flavor;
    tinfo->hashCode = hashCode;
    gdata->traceInfoCount++;
    hashIndex = (int)(hashCode & HASH_INDEX_MASK);
    tinfo->next = gdata->hashBuckets[hashIndex];
    gdata->hashBuckets[hashIndex] = tinfo;
    tinfo->historyP = NULL;
  }
  return tinfo;
}

/* Create hash code for a Trace */
static jlong
hashTrace(Trace *trace)
{
  jlong hashCode;
  int i;
  hashCode = 0;
  for (i = 0; i < trace->nframes; i++) {
    hashCode = (hashCode << 3) +
      (jlong)(ptrdiff_t)(void*)(trace->frames[i].method);
    hashCode = (hashCode << 2) +
      (jlong)(trace->frames[i].location);
  }
  hashCode = (hashCode << 3) + trace->nframes;
  hashCode += trace->flavor;
  return hashCode;
}

/* Lookup or create a new TraceInfo */
static TraceInfo *
lookupOrEnter(jvmtiEnv *jvmti, Trace *trace, TraceFlavor flavor)
{
  TraceInfo *tinfo;
  jlong hashCode;

  /* Calculate hash code (outside critical section to lessen contention */
  hashCode = hashTrace(trace);

  /* Do a lookup in the hash table */
  enterCriticalSection(jvmti); {
    TraceInfo *prev;
    int hashIndex;

    /* Start with first item in hash buck chain */
    prev = NULL;
    hashIndex = (int)(hashCode & HASH_INDEX_MASK);
    tinfo = gdata->hashBuckets[hashIndex];
    while (tinfo != NULL) {
      if (tinfo->hashCode == hashCode &&
  	  memcmp(trace, &(tinfo->trace), sizeof(trace))==0) {
  	/* We found one that matches, move to head of bucket chain */
  	if (prev != NULL) {
  	  /* Remove from list and add to head of list */
  	  prev->next = tinfo->next;
  	  tinfo->next = gdata->hashBuckets[hashIndex];
  	  gdata->hashBuckets[hashIndex] = tinfo;
  	}
  	break;
      }
      prev = tinfo;
      tinfo = tinfo->next;
    }

    /* If we didn't find anything we need to enter a new entry */
    if (tinfo == NULL) {
      /* create new hash table element */
      tinfo = newTraceInfo(trace, hashCode, flavor);
    }

    /*Update stats */
    (void)updateStats(tinfo);
    
  } exitCriticalSection(jvmti);

  return tinfo;
}




/* Get TraceInfo for this allocation */
static TraceInfo*
findTraceInfo(jvmtiEnv *jvmti, jthread thread, TraceFlavor flavor)
{

  TraceInfo *tinfo;
  jvmtiError error;
  tinfo = NULL;
  // printf("Enter findtrack()\n");
  if (thread != NULL) {
   
    static Trace empty;
    Trace trace;

    /* before VN_INIT thread could be NULL, watch out */
    trace = empty;
    error = (*jvmti)->GetStackTrace(jvmti, thread, 0, MAX_FRAMES+2,
				    trace.frames, &(trace.nframes));
   
    /* If we get a PHASE error, the VM isn't ready, or it died */
    if (error == JVMTI_ERROR_WRONG_PHASE) {
      /* It is assumed this is before VM_INIT */
      if (flavor == TRACE_USER) {
	tinfo = emptyTrace(TRACE_BEFORE_VM_INIT);
      } else {
	tinfo = emptyTrace(flavor);
      }
    } else {
      /* If thread == NULL, it's assumed this is before VM_START */
      check_jvmti_error(jvmti, error, "Cannot get stack trace");
      /* Lookup this entry */
      tinfo = lookupOrEnter(jvmti, &trace, flavor);
    }
  } else {
    // printf("yes NULL\n");
    /* If thread==NULL, it's assumed this is before VM_START */
    if (flavor == TRACE_USER) {
      tinfo = emptyTrace(TRACE_BEFORE_VM_START);
    } else {
      tinfo = emptyTrace(flavor);
    }
  }
  return tinfo;
}



/* Tag an object with a TraceInfo pointer.*/
static void
tagObjectWithTraceInfo(jvmtiEnv *jvmti, jobject object, TraceInfo *tinfo)
{
  jvmtiError error;
  jlong tag;

  /* Tag this object with this TraceInfo pointer */
  tag = (jlong)(ptrdiff_t)(void*)tinfo;
  error = (*jvmti)->SetTag(jvmti, object, tag);
  check_jvmti_error(jvmti, error, "Cannot tag object");
}

//bobo
/* Tag object with corresponding otag.*/
static void
tagObject(jvmtiEnv* jvmti, jobject object, OTag* otag)
{
  if (gdata->vmDead) return;
  
  jvmtiError error;
  jlong tag;

  /* Tag this object with this OTag pointer */
  tag = (jlong)(ptrdiff_t)(void*)otag;
  error = (*jvmti)->SetTag(jvmti, object, tag);
  check_jvmti_error(jvmti, error, "Cannot tag object with OTag");
}


/* Java Native Method for Object.<init> */
static void JNICALL
HEAP_TRACKER_native_newobj(JNIEnv *env, jclass klass, jthread thread, jobject o)
{
  if (gdata->vmDead) {
    return;
  }

  TraceInfo *tinfo;
  
  tinfo = findTraceInfo(gdata->jvmti, thread, TRACE_USER);
  // tagObjectWithTraceInfo(gdata->jvmti, o, tinfo);

  OTag* otag = (OTag*)calloc(1, sizeof(OTag));
  otag->tinfoP = tinfo;
  otag->whereCreated = 1; // this object is created from "newobj"
  otag->currentValue = NULL;
  otag->panalty = 0;

  tagObject(gdata->jvmti, o, otag);

  
}


/* Java native method for newarray */
static void JNICALL
HEAP_TRACKER_native_newarr(JNIEnv *env, jclass klass, jthread thread, jobject a)
{
  if (gdata->vmDead) return;

  TraceInfo* tinfo;

  tinfo = findTraceInfo(gdata->jvmti, thread, TRACE_USER);

  OTag* otag = (OTag*)calloc(1, sizeof(OTag));
  otag->tinfoP = tinfo;
  otag->whereCreated = 2;
  otag->currentValue = NULL;
  otag->panalty = 0;

  tagObject(gdata->jvmti, a, otag);
  
}


/*
 * Callback we get when the JVM starts up, but before its initialized. ==================
 * Sets up the JNI calls.
 */
static void JNICALL
cbVMStart(jvmtiEnv *jvmti, JNIEnv *env)
{

    
  //    static JNINativeMethod registry[2] = {{"_newobj",
  //  "(Ljava/lang/Object;Ljava/lang/Object;)V",
  //  (void*) &setObjExpression},
  // {"_getTag", "(Ljava/lang/Object;)Ljava/lang/Object;",
  // (void*) &getObjExpression}};


  //    jclass klass;
  //    klass = env->FindClass("edu/uci/bojunw2/tagging/Tagger");

  // if (klass == NULL) {
  //   fatal_error("ERROR: JNI: Cannot find Tagger with findClass\n");
  // }

  // jint rc;
  // rc = env-> RegisterNatives(klass, registry, 2);
  // if (rc != 0) {
  //   fatal_error("ERROR: JNI: Cannot register natives methods for Tagger\n");
  // }
  // /* Engage calls. */
  // jfieldID field;
  // field = env->GetStaticFieldID(klass, "engaged", "I");
  // if (field == NULL) {
  //   fatal_error("ERROR: JNI: Cannot get field\n");
  // }
  // env->SetStaticIntField(klass, field, 1);
	
  // /* Indicate VM has started */
  // gdata->vm_is_started = JNI_TRUE;
  //bobo	exit_critical_section(jvmti);
  //     gdata->vmStarted = JNI_TRUE;
  //     printf("bobo-VM is started!\n");
    
  //   } exitCriticalSection(jvmti);
	

  ////////////////////////////////////////////////////////////////////////////////////////////
  enterCriticalSection(jvmti); {
    jclass klass;
    jfieldID field;
    jint rc;

    /* Java Native Methods for class */
    static JNINativeMethod registry[2] = {
      {STRING(HEAP_TRACKER_native_newobj), "(Ljava/lang/Object;Ljava/lang/Object;)V",
       (void*)&HEAP_TRACKER_native_newobj},
      {STRING(HEAP_TRACKER_native_newarr), "(Ljava/lang/Object;Ljava/lang/Object;)V",
       (void*)&HEAP_TRACKER_native_newarr}
    };

    /* Register Natives for class whose methods we use */
    klass =(*env)->FindClass(env, STRING(HEAP_TRACKER_class));
    
    if ( klass == NULL ) {
      fatal_error("ERROR: JNI: Cannot find %s with FindClass\n",
		  STRING(HEAP_TRACKER_class));
    }
    printf("goes here`````\n");
    printf("Reached here: haha!\n");
    rc = (*env)->RegisterNatives(env, klass, registry, 2);
    if ( rc != 0 ) {
      fatal_error("ERROR: JNI: Cannot register natives for class %s\n",
		  STRING(HEAP_TRACKER_class));
    }

    /* Engage calls. */
    field = (*env)->GetStaticFieldID(env, klass, STRING(HEAP_TRACKER_engaged), "I");
    if ( field == NULL ) {
      fatal_error("ERROR: JNI: Cannot get field from %s\n",
		  STRING(HEAP_TRACKER_class));
    }
    (*env)->SetStaticIntField(env, klass, field, 1);

    /* Indicate VM has started */
    gdata->vmStarted = JNI_TRUE;
    printf("bobo-VM is started!\n");
    printf("JNI_TRUE: %d\n", JNI_TRUE);

  } exitCriticalSection(jvmti);

}




/* Iterate Through Heap Callback */
static jint JNICALL
cbObjectTagger(jlong class_tag, jlong size, jlong *tag_ptr, jint length, void *user_data)
{
  TraceInfo *tinfo;

  tinfo = emptyTrace(TRACE_BEFORE_VM_INIT);
  // *tag_ptr = (jlong)(ptrdiff_t)(void*)tinfo;
  OTag* otag = (OTag*) calloc(1, sizeof(OTag));
  otag->tinfoP = tinfo;
  
  otag->whereCreated = 0; //for debug
  otag->currentValue = NULL;
  otag->panalty = 0;  
  //bobo *tag_ptr = (jlong)(ptrdiff_t)(void*)tinfo;
  *tag_ptr = (jlong)(ptrdiff_t)(void*)otag; //bobo
  return JVMTI_VISIT_OBJECTS;
}


/*bobo-Create a new jthread. */
static jthread
alloc_thread(JNIEnv* env)
{
  jclass thrClass;
  jmethodID cid;
  jthread res;

  thrClass = (*env)->FindClass(env, "java/lang/Thread");
  if (thrClass == NULL) {
    fatal_error("Cannot find Thread class\n");
  }
  cid = (*env)->GetMethodID(env, thrClass, "<init>", "()V");
  if (cid == NULL) {
    fatal_error("Cannot find Thread constructor method\n");
  }
  res = (*env)->NewObject(env, thrClass, cid);
  if (res == NULL) {
    fatal_error("Cannot create new Thread object\n");
  }
  return res;
}

//bobo- 1) first, get each object's otag primitive type field's values
static void JNICALL
cbPrimitiveField(jvmtiHeapReferenceKind kind,
		 const jvmtiHeapReferenceInfo* info,
		 jlong object_class_tag,
		 jlong* object_tag_ptr,
		 jvalue value,
		 jvmtiPrimitiveType value_type,
		 void* user_data)
{
  //not include static fields;
  if (kind == JVMTI_HEAP_REFERENCE_STATIC_FIELD) {
    return;
  }

  if (*object_tag_ptr == 0) {
    return;
  }

  jlong tagp = *object_tag_ptr;

  OTag* otag;
  otag = (OTag*)(void*)(ptrdiff_t)tagp;  //bobo-get object's otag;
  int debugInfo = otag->whereCreated = 1;

  TraceInfo* tinfo = otag->tinfoP;
  if (tinfo == NULL) {
    printf("2\n");
    return;
  }

  FieldPair* fpP = (FieldPair*)calloc(1, sizeof(FieldPair));
  fpP->id = *info;
  fpP->value = value;

  if (otag->currentValue == NULL) {
    ObjValue* objValue = (ObjValue*)calloc(1, sizeof(ObjValue));
    objValue->fields = (FieldPair**)calloc(1, sizeof(FieldPair*));
    objValue->size = 1;
    objValue->fields[0] = fpP;
    otag->currentValue = objValue;
  } else {
    ObjValue* objValue = otag->currentValue;
    int currentIndex = fpP->id.field.index;
    int existIndex = -1;
    jboolean sameID = JNI_FALSE;
    for (int i = 0; i < objValue->size; ++i) {
      existIndex = (objValue->fields[i]->id).field.index;
      if (currentIndex == existIndex) {
	sameID = JNI_TRUE;
	objValue->fields[i]->value = value;
      }
    }

    if (!sameID) {
      objValue->size++;
      FieldPair** pairs = (FieldPair**)calloc(objValue->size, sizeof(FieldPair*));
      // bobo- copy the old Fileds values to the new one;
      for (int i = 0; i < objValue->size-1; ++i) {
	pairs[i] = objValue->fields[i];
      }
      pairs[objValue->size-1] = fpP;
      free(objValue->fields);
      objValue->fields = pairs;
      otag->currentValue = objValue;
    }
  }
  
}


static jboolean JNICALL
isSameJvalue(jvalue v1, jvalue v2) {
  return (v1.z == v2.z
	  && v1.b == v2.b
	  && v1.c == v2.c
	  && v1.s == v2.s
	  && v1.i == v2.i
	  && v1.j == v2.j
	  && v1.f == v2.f
	  && v1.d == v2.d
	  && v1.l == v2.l) ? JNI_TRUE : JNI_FALSE;
}

static jboolean JNICALL
isSameValue(ObjValue* valueP, ObjValue* valuePInHistory) {
  int sizeP = valueP->size;
  int sizePH = valuePInHistory->size;
  if (sizeP != sizePH) return JNI_FALSE;

  for (int i = 0; i < sizeP; i++) {
    FieldPair* p1 = valueP->fields[i];
    FieldPair* p2 = valuePInHistory->fields[i];
    if (p1->id.field.index == p2->id.field.index) 
      if (isSameJvalue(p1->value,p2->value))  return JNI_TRUE;
  }
  return JNI_FALSE;
}



/* Frame to text */
static void
frameToString(jvmtiEnv *jvmti, char *buf, int buflen, jvmtiFrameInfo *finfo)
{
    jvmtiError           error;
    jclass               klass;
    char                *signature;
    char                *methodname;
    char                *methodsig;
    jboolean             isNative;
    char                *filename;
    int                  lineCount;
    jvmtiLineNumberEntry*lineTable;
    int                  lineNumber;

    /* Initialize defaults */
    buf[0]     = 0;
    klass      = NULL;
    signature  = NULL;
    methodname = NULL;
    methodsig  = NULL;
    isNative   = JNI_FALSE;
    filename   = NULL;
    lineCount  = 0;
    lineTable  = NULL;
    lineNumber = 0;

   
    /* Get jclass object for the jmethodID */
    error = (*jvmti)->GetMethodDeclaringClass(jvmti, finfo->method, &klass);
    check_jvmti_error(jvmti, error, "Cannot get method's class");

    /* Get the class signature */
    error = (*jvmti)->GetClassSignature(jvmti, klass, &signature, NULL);
    check_jvmti_error(jvmti, error, "Cannot get class signature");

    /* Skip all this if it's our own Tracker method */
    if ( strcmp(signature, "L" STRING(HEAP_TRACKER_class) ";" ) == 0 ) {
        deallocate(jvmti, signature);
        return;
    }

    /* Get the name and signature for the method */
    error = (*jvmti)->GetMethodName(jvmti, finfo->method,
                &methodname, &methodsig, NULL);
    check_jvmti_error(jvmti, error, "Cannot method name");

    /* Check to see if it's a native method, which means no lineNumber */
    error = (*jvmti)->IsMethodNative(jvmti, finfo->method, &isNative);
    check_jvmti_error(jvmti, error, "Cannot get method native status");

    /* Get source file name */
    error = (*jvmti)->GetSourceFileName(jvmti, klass, &filename);
    if ( error != JVMTI_ERROR_NONE && error != JVMTI_ERROR_ABSENT_INFORMATION ) {
        check_jvmti_error(jvmti, error, "Cannot get source filename");
    }

    /* Get lineNumber if we can */
    if ( !isNative ) {
        int i;

        /* Get method line table */
        error = (*jvmti)->GetLineNumberTable(jvmti, finfo->method, &lineCount, &lineTable);
        if ( error == JVMTI_ERROR_NONE ) {
            /* Search for line */
            lineNumber = lineTable[0].line_number;
            for ( i = 1 ; i < lineCount ; i++ ) {
                if ( finfo->location < lineTable[i].start_location ) {
                    break;
                }
                lineNumber = lineTable[i].line_number;
            }
        } else if ( error != JVMTI_ERROR_ABSENT_INFORMATION ) {
            check_jvmti_error(jvmti, error, "Cannot get method line table");
        }
    }

    /* Create string for this frame location.
     *    NOTE: These char* quantities are mUTF (Modified UTF-8) bytes
     *          and should actually be converted to the default system
     *          character encoding. Sending them to things like
     *          printf() without converting them is actually an I18n
     *          (Internationalization) error.
     */
    (void)sprintf(buf, "%s.%s@%d[%s:%d]",
            (signature==NULL?"UnknownClass":signature),
            (methodname==NULL?"UnknownMethod":methodname),
            (int)finfo->location,
            (filename==NULL?"UnknownFile":filename),
            lineNumber);

    /* Free up JVMTI space allocated by the above calls */
    deallocate(jvmti, signature);
    deallocate(jvmti, methodname);
    deallocate(jvmti, methodsig);
    deallocate(jvmti, filename);
    deallocate(jvmti, lineTable);
}





/* print the information */
static void
printTraceInfo(jvmtiEnv *jvmti, int index, TraceInfo *tinfo)
{
  if (tinfo == NULL) {
    fatal_error("%d: NULL ENTRY ERROR\n", index);
    return;
  }

  stdout_message("%2d: %7d bytes %5d objects %5d live %s",
		 index, (int)tinfo->totalSpace, tinfo->totalCount,
		 tinfo->useCount, flavorDesc[tinfo->trace.flavor]);

  if (tinfo->trace.nframes > 0) {
    int i;
    int fcount;

    fcount = 0;
    stdout_message(" stack=(");

    for(i = 0; i < tinfo->trace.nframes; i++) {
      char buf[4096];

      frameToString(jvmti, buf, (int)sizeof(buf), tinfo->trace.frames+i);
      if (buf[0] == 0) {
	continue;
      }
      fcount++;
      stdout_message("%s", buf);
      if (i < (tinfo->trace.nframes-1)) {
	stdout_message(",\n");
      }
    }
    stdout_message(") nframes=%d\n", fcount);
  } else {
    stdout_message(" stack=<empty>\n");
  }
}




/* bobo-  2) second, iterate every object and compare its value with history */
static void JNICALL
cbIterateObj(jlong class_tag,
	     jlong size,
	     jlong* tag_ptr,
	     jint length,
	     void* user_data)
{
 
  
  gdata->totalObjNum++;
#ifdef DEBUG
  printf("Obj #: %d\n", gdata->totalObjNum);
#endif
  jlong tagP = *tag_ptr;
  OTag* otag = (OTag*)(void*)(ptrdiff_t)tagP;

  ObjValue* currentValue =  otag->currentValue;
  if (currentValue == NULL) {
    // printf("this obj doesn't have primitive type fields\n");
    return;
  }

   //bobo-debug2: only add the size of the objs who have primitive-type fields
  /* printf("this obj has primitive type.\n"); */
  /* gdata->heapSize += size; */
  /* printf("in gc# %d \nheapSize = %d\n", gdata->gc_number, gdata->heapSize); */

  TraceInfo* tinfoP = otag->tinfoP;
  History* historyP = tinfoP->historyP;
  if (!historyP) {
    #ifdef DEBUG
    printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
    printf("Obj %d:\n", gdata->totalObjNum);
    #endif
    
    historyP = (History*)calloc(1, sizeof(History));
    ObjValue** valuesP = (ObjValue**)calloc(1, sizeof(ObjValue*));
    valuesP[0] = currentValue;
    historyP->valuesP = valuesP;
    historyP->size = 1;
    tinfoP->historyP = historyP;
  } else {
    ObjValue** valuesP = historyP->valuesP;
    int historySize = historyP->size;

    //bobo-debug: print the values in the history

    printf("obj %d has history:\n", gdata->totalObjNum);
    
    //bobo- find whether history has the same value
    for(int i = 0; i < historySize; ++i) {
      ObjValue* valuePInHistory = valuesP[i];
      //bobo-debug:
      
      /* int fieldNum = valuePInHistory->size; */

      
      /* printf("Obj %d's fields:\n", gdata->totalObjNum); */
      /* for (int j = 0; j < fieldNum; ++j) { */
      /* 	printf("in field id = %d, the value = %d\n", (valuePInHistory->fields[j]->id).field.index, valuePInHistory->fields[j]->value); */
      /* } */
      
      // printf("in field = %d, the value = %d", valuePInHistory->field
      printf("check: is same value\n");
      if (isSameValue(currentValue, valuePInHistory)) {
	 printf("same value: add panalty\n");
	 otag->panalty += size;
	 //bobo-debug2
	 printf("size = %d, panalty = %d\n", size, otag->panalty);
  
	 return;
      }
    }
    // different with values in history
    // means this object isn't same as in history, clean panalty
    
    otag->panalty = 0;
    
    int hSize = ++historyP->size;
    
    if (hSize <= 10) {

      printf("@@@@@@@@@@@@ history size = %d @@@@@@@@@@@@@\n", hSize);

      //bobo-debug
    
      ObjValue** newValuesP = (ObjValue**)calloc(hSize, sizeof(ObjValue*));
      for (int i = 0; i < hSize-1; ++i) {
	newValuesP[i] = valuesP[i];
      }
      newValuesP[hSize-1] = currentValue;
      free(historyP->valuesP);
      historyP->valuesP = newValuesP;
    } else {
      printf("History size > 10, keep m = 10!\n");
      
      for (int i = 0; i < historyP->size; ++i) {
	valuesP[i] = valuesP[i + 1];	
      }
      valuesP[historyP->size] = currentValue;   
    }     
  }
}


static void
lookupOrEnterTraceInfoR(TraceInfo* tinfoP,
			int panalty) {
  TraceInfoRecord* traceInfoR = (TraceInfoRecord*)calloc(1, sizeof(TraceInfoRecord));
  traceInfoR->tinfoP =tinfoP;
  traceInfoR->panalty = panalty;
  traceInfoR->next = NULL;

  if (gdata->traceInfoTable == NULL) {
    gdata->traceInfoTable = (TraceInfoRecord**)calloc(1, sizeof(TraceInfoRecord*));
    *(gdata->traceInfoTable) = traceInfoR;
	    
  } else {
    TraceInfoRecord* temp, *prev;
    temp = *(gdata->traceInfoTable);
    while(temp) {

      //if tinfo has already existed, update the panalty;
      if (temp->tinfoP == traceInfoR->tinfoP) {
	if (temp->panalty < traceInfoR->panalty) {
	  temp->panalty = traceInfoR->panalty;
	}
	break;
      }
      prev = temp;
      temp = temp->next;
    }

    //when tinfo doesn't exist, insert this traceInfoR;
    if (temp == NULL) {
      prev->next = traceInfoR;
    }
  }    
}

// 3)
//iterate through the heap and remove the previous fields' values;
static void JNICALL
cbIterateObjRemove(jlong class_tag,
		   jlong size,
		   jlong* tag_ptr,
		   jint length,
		   void* user_data) {
  gdata->totalObjNum3++;
  jlong tagP = *tag_ptr;
  OTag* otag = (OTag*)(void*)(ptrdiff_t)tagP;

  //bobo-debug2
  // printf("in cbIterateObjRemove(), iterate all objects again, count hs and total sum of PC.\n");
  
  if (otag-> panalty > 0) {
    ++gdata->tempCount;
    if (gdata->tempCount == 1) {
      printf("@@@@@ before 3) iteration,tempCount =%d, gdata->heapSize = %d, gdata->totalPanalty = %d\n",gdata->tempCount, gdata->heapSize, gdata->totalPanalty);
    }
    printf("****otag->panalty = %d, size = %d\n", otag->panalty, size);
    gdata->heapSize += size;
    gdata->totalPanalty += otag->panalty;
    printf("****gdata->heapSize = %d, gdata->totalPanalty = %d\n", gdata->heapSize, gdata->totalPanalty);
  }

  if (!otag->currentValue) {
    free(otag->currentValue);
    otag->currentValue = NULL;
  }

  /* /\* bobo- now not only report bad allocation site at vmDeath, also report at each gc*\/ */
  /* if (otag->panalty > gdata->PANALTY_THRESHOLD) { */
  /*   if ((otag->tinfoP->trace).flavor == TRACE_USER) { */
  /*     gdata->bad_alloc_num++; */
  /*     printf("******************************flavor: %d, nframes: %d*************************************\n", (otag->tinfoP->trace).flavor, (otag->tinfoP->trace).nframes); */
  /*     printf("############bad allocation site:################\n"); */
  /*     printf("In this allocation site, the number of the same obj: %d\n", otag->tinfoP->sameObj); */
  /*     printTraceInfo(gdata->jvmti, gdata->bad_alloc_num, otag->tinfoP); */
  /*   } */

/* bobo- now not only report bad allocation site at vmDeath, also report at each gc*/ 
  if (otag->panalty > gdata->PANALTY_THRESHOLD) {
    gdata->badObjNum++;
    // record the tinfoP and panalty

    lookupOrEnterTraceInfoR(otag->tinfoP, otag->panalty);

    
    /* TraceInfoRecord* traceInfoR = (TraceInfoRecord*)calloc(1, sizeof(TraceInfoRecord)); */
    /* traceInfoR->tinfoP = otag->tinfoP; */
    /* traceInfoR->panalty = otag->panalty; */
    /* traceInfoR->next = NULL; */
    
    /* if (gdata->traceInfoTable == NULL) { */
    /*  gdata->traceInfoTable = (TraceInfoRecord**) calloc(1, sizeof(TraceInfoRecord*)); */
    /*  *(gdata->traceInfoTable) = traceInfoR; */
    /* } else { */
    /*   TraceInfoRecord* temp, *prevP; */
    /*   temp = *(gdata->traceInfoTable); */
    /*   while(temp) { */
	
    /* 	// if tinfoP has already existed, update the panalty; */
    /* 	if (temp-> tinfoP == traceInfoR->tinfoP) { */
    /* 	  if ( temp->panalty < traceInfoR->panalty) { */
    /* 	    temp->panalty = traceInfoR->panalty; */
    /* 	  } */
	  
    /* 	  break; */
    /* 	} */
    /* 	prevP = temp; */
    /* 	temp = temp->next; */
    /*   } */
    /*   // when tinfoP doesn't exist, insert this traceInfoR; */
    /*   if (temp == NULL) { */
    /* 	prevP->next = traceInfoR; */
	
    /*   } */
     
      
    /* } */
  }

}

void cleanVSOData() {
  //clean the heapSize and totalPanalty at each round of gc;
  gdata->heapSize = 0;
  gdata->totalPanalty = 0;
  printf("after clean value, heapSize = %d, totalPanalty = %d\n", gdata->heapSize, gdata->totalPanalty);
}


/* bobo- worker function for arbitary call after each GC */
static void JNICALL
worker(jvmtiEnv* jvmti, JNIEnv* jni, void* p)
{
  jvmtiError error;

  stdout_message("GC worker starts ...\n");

  for(;;) {
    error = (*jvmti)->RawMonitorEnter(jvmti, gdata->lock2);
    check_jvmti_error(jvmti, error, "raw monitor enter");
    while (gdata->gc_count == 0) {
      error = (*jvmti)->RawMonitorWait(jvmti, gdata->lock2, 0);
      if (error != JVMTI_ERROR_NONE) {
	error = (*jvmti)->RawMonitorExit(jvmti, gdata->lock2);
	check_jvmti_error(jvmti, error, "raw monitor wait");
	return;
      }
    }

    enterCriticalSection(jvmti);
    {
      gdata->gc_count = 0;

      // error = (*jvmti)->RawMonitorExit(jvmti, gdata->lock2);
      //check_jvmti_error(jvmti, error, "raw monitor exit");

      // Now can do JVMTI/JNI functions after GC
      printf("three rounds for iterate the heap:\n");

      jvmtiHeapCallbacks heapCallbacks;
  
      printf("---------------1) entering cbPrimitiveField------------\n");
      //bobo- 1) first, get each object's otag primitive type field's values
      (void)memset(&heapCallbacks, 0, sizeof(heapCallbacks));
      heapCallbacks.primitive_field_callback = &cbPrimitiveField;
      error = (*jvmti)->IterateThroughHeap(jvmti, JVMTI_HEAP_FILTER_UNTAGGED,
					   NULL, &heapCallbacks, NULL);
      check_jvmti_error(jvmti, error, "Cannot iterate through heap");

      printf("--------------2) entering cbIterateObj()--------------\n");
      //bobo- 2) second, iterate every object and compare its value with history
      (void)memset(&heapCallbacks, 0, sizeof(heapCallbacks));
      heapCallbacks.primitive_field_callback = NULL;
      heapCallbacks.heap_iteration_callback = &cbIterateObj;
      error = (*jvmti)->IterateThroughHeap(jvmti, JVMTI_HEAP_FILTER_UNTAGGED,
					   NULL, &heapCallbacks, NULL);
      check_jvmti_error(jvmti, error, "Cannot iterate through heap");
    

      printf("round2: total objects number: %d\n", gdata->totalObjNum);
      gdata->totalObjNum = 0;
 

      printf("--------------3) entering cbIterateObjRemove()--------------\n");
      cleanVSOData();
      //bobo- 3) third, iterate every object and remove the previous fields' values;
      (void)memset(&heapCallbacks, 0, sizeof(heapCallbacks));
      heapCallbacks.primitive_field_callback = NULL;
      heapCallbacks.heap_iteration_callback = &cbIterateObjRemove;
      error = (*jvmti)->IterateThroughHeap(jvmti, JVMTI_HEAP_FILTER_UNTAGGED,
					   NULL, &heapCallbacks, NULL);
      check_jvmti_error(jvmti, error, "Cannot iterate through heap");

      //bobo-debug2
      gdata->tempCount = 0;
      printf("after cbIterateRemove,temp(cleaned) = %d, heapSize = %d, totalPanalty = %d\n", gdata->tempCount, gdata->heapSize, gdata->totalPanalty);
      double vso = (double)(gdata->heapSize + gdata->totalPanalty) / gdata->heapSize;
      printf("vso = %f \n", vso);
      if (gdata->maxVSO < vso) {
	gdata->maxVSO = vso;
      }
      printf("in 3) interation, compute VSO, and maxVSO = %f, then do clean.\n", gdata->maxVSO);
      //clean the heapSize and totalPanalty at each round of gc;
      cleanVSOData();


      printf("round 3: total objects number: %d\n", gdata->totalObjNum3);
      printf("round 3: bad objects number: %d\n", gdata->badObjNum3);
      gdata->totalObjNum3 = 0;
      gdata->badObjNum = 0;
 
    
      error = (*jvmti)->RawMonitorExit(jvmti, gdata->lock2);
      check_jvmti_error(jvmti, error, "raw monitor exit");

    } exitCriticalSection(jvmti);

  }
}


/*
 * Callback we get when the JVM is initialized. We use this time to setup our GC thread
 */
static void JNICALL cbVMInit(jvmtiEnv * jvmti, JNIEnv * env, jthread thread)
{
  jvmtiHeapCallbacks heapCallbacks;
  jvmtiError error;

  /* bobo-1) start the worker to do some post-GarbageCollectionFinish action when each GC ends.*/
  error = (*jvmti)->RunAgentThread(jvmti, alloc_thread(env), &worker, NULL,
				   JVMTI_THREAD_MAX_PRIORITY);
  check_jvmti_error(jvmti, error, "running agent thread");
  

  /* Iterate through heap, find all untagged objects allocated before this */
  (void)memset(&heapCallbacks, 0, sizeof(heapCallbacks));
  heapCallbacks.heap_iteration_callback = &cbObjectTagger;
  error = (*jvmti)->IterateThroughHeap(jvmti, JVMTI_HEAP_FILTER_TAGGED,
				       NULL, &heapCallbacks, NULL);
  check_jvmti_error(jvmti, error, "Cannot iterate through heap");

  enterCriticalSection(jvmti); {

    /* Indicate VM is initialized */
    gdata->vmInitialized = JNI_TRUE;
  } exitCriticalSection(jvmti);
  printf("VM is initiated!\n");
}


/* Iterate Through Heap callback */
static jint JNICALL
cbObjectSpaceCounter(jlong class_tag, jlong size, jlong* tag_ptr, jint length,
                     void *user_data)
{
  
  TraceInfo *tinfo;

  /* change to output only the bad alloction site */
  /* bobo  tinfo = (TraceInfo*)(ptrdiff_t)(*tag_ptr); */
    
  /* if ( tinfo == NULL ) { */
  /*     tinfo = emptyTrace(TRACE_MYSTERY); */
  /*     *tag_ptr = (jlong)(ptrdiff_t)(void*)tinfo; */
  /* } */
  /* tinfo->totalSpace += size; */
    
  //bobo
  OTag* otag = (OTag*)(ptrdiff_t)(*tag_ptr);
  if (otag != NULL) {

    //bobo-debug2
    // printf("this obj has otag.\n");
    //gdata->heapSize += size;
    // printf("cbObjectSpaceCounter, hs = %d", gdata->heapSize);
    
    //bobo-modify2
    int panalty = otag->panalty;
#ifdef DEBUG
    printf("in. panalty: %d\n", panalty);
#endif
      
    //if (panalty >= gdata->PANALTY_THRESHOLD) {
    if (panalty > 0) {
    
      TraceInfo* tinfoP = otag->tinfoP;
      tinfoP->sameObj++;
      printf("here is a obj, whoes panalty is: %d\n", panalty);
      //bobo-debug2
      gdata->totalPanalty += panalty;
      gdata->heapSize += size;
      printf("in vmdeath, totalpanalty = %d\n", gdata->totalPanalty);

      lookupOrEnterTraceInfoR(tinfoP, panalty);
	
    }
      
  }
#ifdef DEBUG
  printf("End of find bad objects\n");
#endif
  if (otag == NULL) {
    otag = (OTag*)calloc(1, sizeof(OTag));
  }
  tinfo = otag->tinfoP;
  if (tinfo == NULL) {
    tinfo = emptyTrace(TRACE_MYSTERY);
  }

  *tag_ptr = (jlong)(ptrdiff_t)(void*)otag;

  tinfo->totalSpace += size;
  return JVMTI_VISIT_OBJECTS;
}




/* qsort compare function */
static int
compareInfo(const void *p1, const void *p2)
{
  TraceInfo *tinfo1, *tinfo2;

  tinfo1 = *((TraceInfo**)p1);
  tinfo2 = *((TraceInfo**)p2);

  return (int)(tinfo2->totalSpace - tinfo1->totalSpace);
}









/*
 * Callback we receive when the JVM terminates - no more functions can be called after this
 */
static void JNICALL cbVMDeath(jvmtiEnv *jvmti, JNIEnv* jni_env) {
  #ifdef DEBUG
  printf("VM is dead~\n"); //bobo
  printf("Object Number is:%d\n",gdata->obj_count);//bobo
  printf( "gc_count: %d\n", gdata->gc_count);
  #endif

  printf("#####################   Entering cbVMDeath()  ################\n");

  jvmtiHeapCallbacks heapCallbacks;
  jvmtiError error;

  /* Purposely done outside the critical section */
  /* Force garbage collection now so we get our ObjectFree calls */
  printf("#####################   enforce gc in cbVMDeath()  ################\n");
  error = (*jvmti)->ForceGarbageCollection(jvmti);
 
  check_jvmti_error(jvmti, error, "Cannot force garbage collection");

  /* Iterate through heap and find all objects */
  (void)memset(&heapCallbacks, 0, sizeof(heapCallbacks));
  printf("##################   Entering cbObjectSpaceCounter(), heapSize = %d, totalPanalty = %d    ################\n", gdata->heapSize, gdata->totalPanalty);
  
  heapCallbacks.heap_iteration_callback = &cbObjectSpaceCounter;
  error = (*jvmti)->IterateThroughHeap(jvmti, 0, NULL, &heapCallbacks, NULL);
  check_jvmti_error(jvmti, error, "Cannot iterate through heap");

  //bobo-debug2
  double vso = (double)(gdata->totalPanalty + gdata->heapSize) / gdata->heapSize;
  printf("in vmdeath, hs = %d, tp = %d\n", gdata->heapSize, gdata->totalPanalty);
  printf("in vmdeath period, final vso = %f\n", vso);
  if (gdata->maxVSO < vso) {
    gdata->maxVSO = vso;
  }
  
  /* Process VM Death */
  enterCriticalSection(jvmti);{
    jclass klass;
    jfieldID field;
    jvmtiEventCallbacks callbacks;

    /* Disengage calls in HEAP_TRACKER_class. */
    klass = (*jni_env)->FindClass(jni_env, STRING(HEAP_TRACKER_class));
    if (klass == NULL) {
      fatal_error("ERROR: JNI: Cannot find %s with FindClass\n",
		  STRING(HEAP_TRACKER_class));
    }
    field = (*jni_env)->GetStaticFieldID(jni_env, klass, STRING(HEAP_TRACKER_engaged), "I");
    if (field == NULL) {
      fatal_error("ERROR: JNI: Cannot get field from %s\n",
		  STRING(HEAP_TRACKER_class));
    }
    (*jni_env)->SetStaticIntField(jni_env, klass, field, 0);

    /* The critical section here is important to hold back the VM death
     *until all other callbacks have completed.
     */

    /* Clear out all callbacks. */
    (void)memset(&callbacks, 0, sizeof(callbacks));
    error = (*jvmti)->SetEventCallbacks(jvmti, &callbacks,
					(jint)sizeof(callbacks));
    check_jvmti_error(jvmti, error, "Cannot set jvmti callbacks");

    gdata->vmDead = JNI_TRUE;

    /* Dump all objects */
    if (gdata->traceInfoCount > 0) {
      TraceInfo **list;
      int count;
      int i;

      stdout_message("Dumping heap trace information\n");

      /* Create single array of pointers to TraceInfo's, sort, and */
/* print top gdata->maxDump top space users. */

      list = (TraceInfo**)calloc(gdata->traceInfoCount,
				 sizeof(TraceInfo*));
      if (list == NULL) {
	fatal_error("ERROR: Ran out of malloc() space\n");
      }
      count = 0;
      for(i = 0; i < HASH_BUCKET_COUNT; i++) {
	TraceInfo *tinfo;

	tinfo = gdata->hashBuckets[i];
	while (tinfo != NULL) {
	  if (count < gdata->traceInfoCount) {
	    list[count++] = tinfo;
	  }
	  tinfo = tinfo->next;
	}
      }
      if (count != gdata->traceInfoCount) {
	fatal_error("ERROR: Count found by iterate doesn't match ours:"
		    "count=%d != traceInfoCount==%d\n",
		    count, gdata->traceInfoCount);
      }
      qsort(list, count, sizeof(TraceInfo*), &compareInfo);
      int j = 0;
      for(i = 0; i < count; i++) {  // need to iterate all the allocation sites
	if (j >= gdata->maxDump) {
	  break;
	}
	/* if (list[i]->sameObj >= gdata->BAD_ALLOCSITE_THRESHOLD) { */
	/*   printf("############    bad allocation site:   ################\n"); */
	/*   printf("In this allocation site, the number of the same obj: %d\n", list[i]->sameObj); */
	/*   printTraceInfo(jvmti, i+1, list[i]); */
	/*   j++; */
	/* } */
      }
      (void)free(list);
       
      if (gdata->traceInfoTable) {
	TraceInfoRecord* temp;
	temp = *(gdata->traceInfoTable);
	int index = 0;
	while (temp){
	  printf("############ from gc-bad allocation site:################\n");
	  printTraceInfo(jvmti, ++index, temp->tinfoP);
	 
	  temp = temp->next;
	}     
      }  
    }
  } exitCriticalSection(jvmti);

  printf("In the end, maxVSO = %f\n", gdata->maxVSO);
}



static void JNICALL cbVMObjectAlloc(jvmtiEnv *jvmti, JNIEnv* jni_env, jthread thread, jobject object, jclass object_klass, jlong size){

  TraceInfo *tinfo;

  if (gdata->vmDead) {
    return;
  }
  tinfo = findTraceInfo(jvmti, thread, TRACE_VM_OBJECT);
  //tagObjectWithTraceInfo(jvmti, object, tinfo);
  //  gdata->obj_count++; //bobo
  OTag* otag = (OTag*)calloc(1, sizeof(OTag));
  otag->tinfoP = tinfo;
  otag->whereCreated = 4; 
  otag->currentValue = NULL;
  otag->panalty = 0;
  tagObject(jvmti, object, otag);
}


 
static void JNICALL
cbObjectFree(jvmtiEnv *jvmti, jlong tag)
{
  TraceInfo *tinfo;
  if (gdata->vmDead) {
    return;
  }

  // tinfo = (TraceInfo*)(void*)(ptrdiff_t)tag;
  OTag* otag = (OTag*)(void*)(ptrdiff_t)tag;
  tinfo = otag->tinfoP;

  
  /* decrement the use count */
  if (tinfo != NULL)
    tinfo->useCount--;

  free(otag);
}

static void JNICALL gc_start(jvmtiEnv *jvmti_env) {
  stdout_message("############# Entering gc_starts(): No. %d gc############\n", gdata->gc_number);
  gdata->gc_number++;

  jvmtiError err;
  /* err = (*jvmti_env)->RawMonitorEnter(jvmti_env, gdata->lock2); */
  /* check_jvmti_error(jvmti_env, err, "raw monitor2 enter"); */
  /* gdata->gc_count++; */
  /* err = (*jvmti_env)->RawMonitorNotify(jvmti_env, gdata->lock2); */
  /* check_jvmti_error(jvmti_env, err, "raw monitor2 notify"); */
  /* err = (*jvmti_env)->RawMonitorExit(jvmti_env, gdata->lock2); */
  /* check_jvmti_error(jvmti_env, err, "raw monitor2 exit"); */
}

static void JNICALL gc_finish(jvmtiEnv *jvmti_env) {
  jvmtiError err;
    
  stdout_message("###################    Entering gc_finish()    #####################\n");

  err = (*jvmti_env)->RawMonitorEnter(jvmti_env, gdata->lock2);
  check_jvmti_error(jvmti_env, err, "raw monitor2 enter");
  gdata->gc_count++;
  err = (*jvmti_env)->RawMonitorNotify(jvmti_env, gdata->lock2);
  check_jvmti_error(jvmti_env, err, "raw monitor2 notify");
  err = (*jvmti_env)->RawMonitorExit(jvmti_env, gdata->lock2);
  check_jvmti_error(jvmti_env, err, "raw monitor2 exit");
}

/* Callback for JVMTI_EVENT_CLASS_FILE_LOAD_HOOK */
static void JNICALL cbClassFileLoadHook(jvmtiEnv *jvmti, JNIEnv *env,
					jclass class_being_redefined, jobject loader,
					const char* name, jobject protection_domain,
					jint class_data_len, const unsigned char* class_data,
					jint *new_class_data_len, unsigned char** new_class_data)
{
  enterCriticalSection(jvmti);{
    //std::cout << "---------------------------------" << std::endl;
    //std::cout << "loader: " << loader << std::endl;
    //std::cout << "name: " << name << std::endl;
    
    
    if (!gdata->vmDead) {
      const char *classname;

      if (name == NULL) {
	classname = java_crw_demo_classname(class_data, class_data_len,
					    NULL);
	if (classname == NULL) {
	  fatal_error("ERROR: No classname in classfile\n");
	}
      }else {
	classname = strdup(name);
	if (classname == NULL) {
	  fatal_error("ERROR: Ran out of malloc() space\n");
	}
      }

      *new_class_data_len = 0;
      *new_class_data = NULL;

      /* Is the class not "HeapTracker.class"  */
      if (strcmp(classname, STRING(HEAP_TRACKER_class)) != 0) {
	jint cnum;
	int systemClass;
	/*get number for each class file image loaded */
	cnum = gdata->ccount++;

	/*Here judge whether it's a system class. If the class is loaded before vmStart,
	 *then it is a system class.
	 */
	systemClass = 0;
	if (!gdata->vmStarted) {
	  systemClass = 1;
	}

	#ifdef DEBUG
	if (!systemClass) {
	   printf("classname: %s\n", classname);
	   // printf("class_data_len: %d\n", class_data_len);
	}
     	#endif

	unsigned char *newImage;  //bobo-return the new class image after BCI
	long newLength;           // see java_crw_demo() constructor

	newImage = NULL;
	newLength = 0;

    	/* Call the class file reader/write demo code */
    	java_crw_demo(cnum, /* Caller assigned class number for class */
    		      classname, /*Internal class name, e.g. java/lang/Object 
    				  */
    		      class_data, /* Pointer to classfile image for this class */
    		      class_data_len, /*Length of the classfile in bytes */
    		      systemClass, /*Set to 1 if thids is a system class
    				    *(prevent injections into empty 
    				    *<clinit>, finalize, and <init> methods) */
    		      STRING(HEAP_TRACKER_class), /*The class that has methods we will call at */
    		                                  /* the injection sites (tclass) */
    		      "L" STRING(HEAP_TRACKER_class) ";", /*Signature of tclass
    							   *must be "L" +tclass_name + ";" */
    		      NULL, /*Method name in tclass to call at offset 0 *
    			     * for every method */
    		      NULL, /* Signature of this call_name method
    			     *(Must be "(II)V") */
    		      NULL, /*Method name in tclass to call at all
    			     * return opcodes in every method */
    		      NULL,  /* Signature of this return_name method 
    			      *(Must be "(II)V")  */
    		      STRING(HEAP_TRACKER_newobj),  /*Method name in tclass to call first thing 
						     * when injecting java.lang.Object.<init> */
    		      "(Ljava/lang/Object;)V",  /*Signature of this obj_init_name method */
		      
    		      STRING(HEAP_TRACKER_newarr),  /*Method name in tclass to call after every
						     *newarr opcode in every method */
    		      "(Ljava/lang/Object;)V", /*Signature
						*/
    		      &newImage, /*return a pointer to new image */
    		      &newLength, /*return the length of the new image */
    		      NULL,
    		      NULL
    		      );

	// 	if we got back a new class image, return it back as "the"
	// 	 *new class image. This must be JVMTI Allocate space.
	//
    	if (newLength > 0) {
    	  unsigned char *jvmti_space;

    	  //bobo-copy the return new class image to JVMTI allocate space;
    	  jvmti_space = (unsigned char *)allocate(jvmti, (jint)newLength);
	  (void)memcpy((void*)jvmti_space, (void*)newImage,(int)newLength);
	  *new_class_data_len = (jint)newLength;
#ifdef DEBUG       
	  printf("The jvmti_space is null: %s\n", (jvmti_space == NULL) ? "true": "false");
#endif	
	  *new_class_data = jvmti_space; 
    	}

	// 	/*always free up the space we get from java_crw_demo() */
    	if (newImage != NULL) {
    	  (void)free((void*)newImage);
	  
    	}	
      }
      
      (void)free((void*)classname); 
    }
  } exitCriticalSection(jvmti);
}




/* Parse the options for this heapTracker agent */
static void
parse_agent_options(char *options)
{
    #define MAX_TOKEN_LENGTH        16
    char  token[MAX_TOKEN_LENGTH];
    char *next;

    /* Defaults */
    gdata->maxDump = 20;

    //bobo: PANALTY_THRESHOLD
    gdata->PANALTY_THRESHOLD = 1;
    gdata->BAD_ALLOCSITE_THRESHOLD = 1;

    /* Parse options and set flags in gdata */
    if ( options==NULL ) {
        return;
    }

    /* Get the first token from the options string. */
    next = get_token(options, ",=", token, (int)sizeof(token));

    /* While not at the end of the options string, process this option. */
    while ( next != NULL ) {
        if ( strcmp(token,"help")==0 ) {
            stdout_message("The heapTracker JVMTI demo agent\n");
            stdout_message("\n");
            stdout_message(" java -agent:heapTracker[=options] ...\n");
            stdout_message("\n");
            stdout_message("The options are comma separated:\n");
            stdout_message("\t help\t\t\t Print help information\n");
            stdout_message("\t maxDump=n\t\t\t How many TraceInfo's to dump\n");
            stdout_message("\n");
            exit(0);
        } else if ( strcmp(token,"maxDump")==0 ) {
            char  number[MAX_TOKEN_LENGTH];

            next = get_token(next, ",=", number, (int)sizeof(number));
            if ( next == NULL ) {
                fatal_error("ERROR: Cannot parse maxDump=number: %s\n", options);
            }
            gdata->maxDump = atoi(number);
        } else if ( token[0]!=0 ) {
            /* We got a non-empty token and we don't know what it is. */
            fatal_error("ERROR: Unknown option: %s\n", token);
        }
        /* Get the next token (returns NULL if there are no more) */
        next = get_token(next, ",=", token, (int)sizeof(token));
    }
}






/*
 * Callback that is notified when our agent is loaded. Registers for event
 * notifications.
 */
JNIEXPORT jint JNICALL Agent_OnLoad(JavaVM *jvm, char *options,
				    void *reserved) {
  static GlobalAgentData     data;
  jvmtiError                 error;
  jint                       res;
  jvmtiEventCallbacks        callbacks;
  jvmtiEnv                   *jvmti = NULL;
  jvmtiCapabilities          capa;

  (void) memset((void*)&data, 0, sizeof(data));
  gdata = &data;

  /* get the jvmti environment */
  res = (*jvm)->GetEnv(jvm, (void**) &jvmti, JVMTI_VERSION_1);
  if (res != JNI_OK || jvmti == NULL) {
    /* This means that the VM was unable to obtain this version of the
     *   JVMTI interface, this is a fatal error.
     */
    printf("ERROR: Unable to access JVMTI Version 1 (0x%x),"
	   " is your J2SE a 1.5 or newer version?"
	   " JNIEnv's GetEnv() returned %d\n", JVMTI_VERSION_1, res);

  }
  
 //save jvmti for later
  gdata->jvmti = jvmti;

  printf("Call function Agent_Onload!hahaha\n"); //bobo
  gdata->obj_count = 0;
  printf("Set obj_count = 0\n");

  
 
	
  // jvmtiEnvP = jvmti; // save in jni.h

  /* Parse any options supplied on java command line */
    parse_agent_options(options);
	
  //Register our capabilities
  (void) memset(&capa, 0, sizeof(jvmtiCapabilities));
  capa.can_signal_thread = 1;
  capa.can_tag_objects = 1;
  capa.can_generate_garbage_collection_events = 1;
  capa.can_generate_vm_object_alloc_events = 1;
  capa.can_generate_object_free_events = 1;
  capa.can_get_source_file_name = 1;
  capa.can_get_line_numbers = 1;
  
  capa.can_generate_all_class_hook_events = 1;

  error =(*jvmti)->AddCapabilities(jvmti, &capa);
  check_jvmti_error(jvmti, error,
		    "Unable to get necessary JVMTI capabilities.");

  //Register callbacks
  (void) memset(&callbacks, 0, sizeof(callbacks));
  callbacks.VMStart = &cbVMStart;
  callbacks.VMInit = &cbVMInit;
  callbacks.VMDeath = &cbVMDeath;
  
  callbacks.VMObjectAlloc = &cbVMObjectAlloc;
  callbacks.ObjectFree = &cbObjectFree;
  
  callbacks.ClassFileLoadHook = &cbClassFileLoadHook;
  callbacks.GarbageCollectionStart = &gc_start;
  callbacks.GarbageCollectionFinish = &gc_finish;
  
	

  error = (*jvmti)->SetEventCallbacks(jvmti, &callbacks, (jint) sizeof(callbacks));
  check_jvmti_error(jvmti, error, "Cannot set jvmti callbacks");

  //Register for events
  error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE, JVMTI_EVENT_VM_START,
					     (jthread) NULL);
  check_jvmti_error(jvmti, error, "Cannot set event notification");
  error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE, JVMTI_EVENT_VM_INIT,
					     (jthread) NULL);
  check_jvmti_error(jvmti, error, "Cannot set event notification");
  error =  (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE, JVMTI_EVENT_VM_DEATH,
					      (jthread) NULL);
  check_jvmti_error(jvmti, error, "Cannot set event notification");

  error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE, 
					     JVMTI_EVENT_VM_OBJECT_ALLOC, NULL);//bobo
  check_jvmti_error(jvmti, error, "Cannot set event notification");

  error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
					     JVMTI_EVENT_OBJECT_FREE, (jthread) NULL);
  check_jvmti_error(jvmti, error, "Cannot set event notification");

  error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
					     JVMTI_EVENT_GARBAGE_COLLECTION_START, NULL);
  check_jvmti_error(jvmti, error, "Cannot set event notification");

  error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
					     JVMTI_EVENT_GARBAGE_COLLECTION_FINISH, NULL);
  check_jvmti_error(jvmti, error, "Cannot set event notification");

  error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
					     JVMTI_EVENT_CLASS_FILE_LOAD_HOOK, NULL);
  check_jvmti_error(jvmti, error, "Cannot set event notification");

  //bobo
  // Ceate raw monitor for post-GarbageCollection actions
  error = (*jvmti)->CreateRawMonitor(jvmti, "lock", &(gdata->lock2));
  check_jvmti_error(jvmti, error, "Cannot create raw monitor2");


  
  /* Create the necessary raw monitor */
  error = (*jvmti)->CreateRawMonitor(jvmti, "lock", &(gdata->lock));
  check_jvmti_error(jvmti, error, "create raw monitor");
	
  /* Create the TraceInfo for various flavors of empty traces */
  TraceFlavor flavor;
  static Trace empty;
  for (flavor = TRACE_FIRST; flavor <= TRACE_LAST; flavor++) {
    gdata->emptyTrace[flavor]=
      newTraceInfo(&empty, hashTrace(&empty), flavor);
  }

  /* Add jar file to boot classpath */
  add_demo_jar_to_bootclasspath(jvmti, "heapTracker");

  /* We return JNI_OK to signify success */
  return JNI_OK;
}
